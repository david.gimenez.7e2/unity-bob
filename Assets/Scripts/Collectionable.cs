﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectionable : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other){
        
        ScoreManager.score += 1;
        Destroy(gameObject);
    }
}
