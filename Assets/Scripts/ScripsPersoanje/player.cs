﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour
{
    private GameManager gameManager;
    private playerMovement dashCount;
    public bool playerInmortal;
    public bool playerBerserk;
    Animator m_Animator;
    public Text berserkText;
    
     void Awake() {
        gameManager = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        dashCount = GameObject.FindGameObjectWithTag("Player").GetComponent<playerMovement>();
        //transform.position = gameManager.lastPos;
    }

    void Start(){
        m_Animator = gameObject.GetComponent<Animator>();
        berserkText.enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "plus"){
            dashCount.hasDashed = false;
            dashCount.isDashing = false;
            other.gameObject.SetActive (false);
        }
        if (other.gameObject.CompareTag("Collectionable")){
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("berserk")){
            playerBerserk = true;
        }
        if (other.tag == "Spikes"){
            
            StartCoroutine(KillPlayerCoroutine());
        }
        if (other.tag == "Slime_Verde"){
            if (!playerInmortal && !playerBerserk)
            {
                other.isTrigger = true;
                StartCoroutine(KillPlayerCoroutine());
            }
            else if (playerBerserk)
            {
                other.isTrigger = true;
                StartCoroutine(KillCoroutine(other));
                berserkText.enabled = false;
            }
            else if (playerInmortal) {
                playerInmortal = false;
            }
        }
        if (other.tag == "Enemy"){
            if (!playerInmortal && !playerBerserk)
            {
                other.isTrigger = true;
                StartCoroutine(KillPlayerCoroutine());
            }
            else if (playerBerserk)
            {
                Destroy(other.gameObject);
                berserkText.enabled = false;
            }
            else if (playerInmortal) {
                playerInmortal = false;
            }
        }
        if (other.tag == "inmoral"){
            playerInmortal = true;
        }
        if (other.tag == "berserk") {
            playerBerserk = true;
            Destroy(other.gameObject);
            berserkText.enabled = true;
        }
    }

    IEnumerator KillCoroutine(Collider2D other){
        m_Animator = other.gameObject.GetComponent<Animator>();
        m_Animator.SetTrigger("death");
        yield return new WaitForSeconds(1);
        Destroy(other.gameObject);
        playerBerserk = false;
    }
    
    IEnumerator KillPlayerCoroutine(){
        this.gameObject.GetComponent<Animator>().SetTrigger("Die");
        yield return new WaitForSeconds(0.5f);
        gameManager.playerDead = true;
    }
}
