﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerMovement : MonoBehaviour
{
    private Animator myAnimator;
    public float speed = 2f;

    public bool canMove;
    public bool wallGrab;
    public bool wallJumped;
    public bool wallSlide;
    public bool isDashing;

    private bool groundTouch;
    public bool hasDashed;
    private Collision coll;

    public int side = 1;

   
    public float jumpForce = 50;
    public float slideSpeed = 5;
    public float wallJumpLerp = 10;
    public float dashSpeed = 20;

    private float weight;
    private Rigidbody2D rb;
    private ForceMode2D force; 
    private bool jump = false;
    public float rcDistance = 0.45f;
    public LayerMask layerGround;
    public LayerMask layerWall;
    public GameObject PlayerGO;

    
    public bool facingRight;



    // Start is called before the first frame update
    void Start()
    {

        facingRight = true;

        coll = PlayerGO.GetComponent<Collision>();

        myAnimator = this.gameObject.GetComponent<Animator>();
        
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x,y);

        Walk(dir);

          if (coll.onWall && Input.GetKey(KeyCode.L) && canMove)
        {
            if(side != coll.wallSide)
                
            wallGrab = true;
            wallSlide = false;
        }

        if (Input.GetKeyUp(KeyCode.W) || !coll.onWall || !canMove)
        {
            wallGrab = false;
            wallSlide = false;
        }

        if (coll.onGround && !isDashing)
        {
            wallJumped = false;
            GetComponent<BetterJump>().enabled = true;
        }
        
        if (wallGrab && !isDashing)
        {
            rb.gravityScale = 0;
            if(x > .2f || x < -.2f)
            rb.velocity = new Vector2(rb.velocity.x, 0);

            float speedModifier = y > 0 ? .5f : 1;

            rb.velocity = new Vector2(rb.velocity.x, y * (speed * speedModifier));
        }
        else
        {
            rb.gravityScale = 3;
        }

        if(coll.onWall && !coll.onGround)
        {
            if (x != 0 && !wallGrab)
            {
                wallSlide = true;
                WallSlide();
            }
        }

        if (!coll.onWall || coll.onGround)
            wallSlide = false;

        if (Input.GetKeyDown(KeyCode.W))
        {

            if (coll.onGround)
                Jump();
            if (coll.onWall && !coll.onGround)
                WallJump();
        }

        if (Input.GetKeyDown(KeyCode.L)  && !hasDashed)
        {
            
            if(xRaw != 0 || yRaw != 0)
                Dash(xRaw, yRaw);
                
        }

        if (coll.onGround && !groundTouch)
        {
            GroundTouch();
            groundTouch = true;
        }

        if(!coll.onGround && groundTouch)
        {
            groundTouch = false;
        }

        /*
       var displacement = new Vector3 (Input.GetAxis("Horizontal"),0);
        //Debug.Log (displacement);
        _rb.transform.Translate(displacement * speed * Time.deltaTime);
        */
        myAnimator.SetFloat("Horizontal",x);
        myAnimator.SetFloat("Vertical",y);
        

        //Debug.Log("Animator horizontal: "+ myAnimator.GetFloat("Horizontal"));
        if(Input.GetKeyDown(KeyCode.W)){
            jump = true;
        }

        if (Input.GetKey(KeyCode.R)){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void Flip(float horizontal){
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight){
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;

            transform.localScale = theScale;
        }
    }


    void Walk(Vector2 dir){
        rb.velocity = (new Vector2(dir.x * speed, rb.velocity.y));
    }

     void FixedUpdate() {

        float horizontal = Input.GetAxis("Horizontal");

        Flip(horizontal);
    
        if(jump && isOnGround()){
            rb.velocity = Vector2.up * jumpForce;
            jump = false;
        }
    }

     bool isOnGround(){
        if(Physics2D.Raycast(rb.transform.position,Vector2.down, rcDistance, layerGround)){
            return true;
        }else
        {
            return false;
        }
    }
    private void Jump(){
        rb.velocity = new Vector2(rb.velocity.x,0);
        rb.velocity += Vector2.up * jumpForce;
    }

     void GroundTouch()
    {
        hasDashed = false;
        isDashing = false;



    }

    private void Dash(float x, float y)
    {
        hasDashed = true;


        rb.velocity = Vector2.zero;
        Vector2 dir = new Vector2(x, y);

        rb.velocity += dir.normalized * dashSpeed;
        StartCoroutine(DashWait());
    }

    IEnumerator DashWait()
    {
        
        rb.gravityScale = 0;
        GetComponent<BetterJump>().enabled = false;
        wallJumped = true;
        isDashing = true;

        yield return new WaitForSeconds(.2f);

      
        rb.gravityScale = 3;
        GetComponent<BetterJump>().enabled = true;
        wallJumped = false;
        isDashing = false;
    }

    IEnumerator GroundDash()
    {
        yield return new WaitForSeconds(.15f);
        if (coll.onGround)
            hasDashed = false;
    }

    private void WallJump()
    {
        if ((side == 1 && coll.onRightWall) || side == -1 && !coll.onRightWall)
        {
            side *= -1;
            
        }

      
        Vector2 wallDir = coll.onRightWall ? Vector2.left : Vector2.right;

        Jump();

        wallJumped = true;
    }

    private void WallSlide()
    {
        if(coll.wallSide != side)
         
        if (!canMove)
            return;

        bool pushingWall = false;
        if((rb.velocity.x > 0 && coll.onRightWall) || (rb.velocity.x < 0 && coll.onLeftWall))
        {
            pushingWall = true;
        }
        float push = pushingWall ? 0 : rb.velocity.x;

        rb.velocity = new Vector2(push, -slideSpeed);
    }

}
