﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAi : MonoBehaviour
{
    public Transform target;
    public float speed = 10f;

    public float nextWaypointDistance = 3f;

    Path path;
    int currentWaypoint = 0;
    bool reachedEndpath = false;

    Seeker seeker;
    Rigidbody2D rb;

    private SpriteRenderer spriteRenderer;


    // Start is called before the first frame update
    void Start() {
        this.spriteRenderer = this.GetComponent<SpriteRenderer>();
    
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        InvokeRepeating("UpdatePath",0f,0.5f);
        
    }

    void FixedUpdate() {

        this.spriteRenderer.flipX = target.transform.position.x < this.transform.position.x;
    }

    void UpdatePath(){
        if(seeker.IsDone()){
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
    }

    void OnPathComplete(Path p){
        if(!p.error){
            path = p;
            currentWaypoint = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (path == null){
            return;
        }
        if (currentWaypoint >= path.vectorPath.Count){
            reachedEndpath = true;
            return;
        }else
        {
            reachedEndpath = false;
        }

        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * speed * Time.deltaTime;

        rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        if(distance < nextWaypointDistance){
            currentWaypoint++;
        }
        
    }
}
