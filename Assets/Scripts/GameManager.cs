﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager _instance;

    public int score;
    public int liveScore;
    public bool playerDead;

    //public Vector2 lastPos;

    void Awake() {
        
        if(_instance == null){
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }else{
            Destroy(this);
        }

    }

    private void Update() {
        if(playerDead){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            ScoreManager.score = 0;
            playerDead = false;
        }
    }
    public void IncrementScore(){
        score ++;

    }  
}
