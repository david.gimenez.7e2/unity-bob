﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParallax : MonoBehaviour
{
    public GameObject camera;
    public float parallax;
    private float bckgrndpos;
    private float length;
    void Start()
    {
        bckgrndpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        float temp = (camera.transform.position.x * (1 - parallax));
        float distance = (camera.transform.position.x * parallax);
        transform.position = new Vector3(bckgrndpos + distance, transform.position.y, transform.position.z);
        if (temp < bckgrndpos - length ){
            bckgrndpos += length;
        }
        else if (temp < bckgrndpos - length){
            bckgrndpos -= length;
        }
    }
}
